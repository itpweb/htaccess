# htaccess + nginx firewall

* Author: [@itpweb](https://gitlab.com/itpweb)
* Author Website: [http://itechplus.org](http://itechplus.org)
* Contributor(s): [@akadusei](https://gitlab.com/akadusei)
* License: [GNU General Public License v2.0 or later](http://www.gnu.org/licenses/gpl-2.0.html)

## Description

This is a `.htaccess` file for apache web servers, with an equivalent for nginx. The file serves as a firewall, ensuring maximum security for your server.

## Usage

Upload the `htaccess` file to, or above, the root directory of your app directory on the web server (eg: `/home/username/public_html/`), and rename to `.htaccess`.

Some code blocks are required to be in the app's root directory, as indicated in their comments. So if the `.htaccess` file is kept above the app's root (eg: `/home/username/`), move any such code to the `.htaccess` file in the app's root.

For nginx, include the nginx-firewall.conf file above all other location rules in all server blocks:
`include /path/to/nginx-firewall.conf;`
